/* Copyright (C) 2024 Marie "armillaire" Z. */
/* Check the included LICENSE file for more information. */

#ifndef UTILS_H_
#define UTILS_H_

/* returns a string which must be freed by the caller. */
char *strip_extra_characters(char *input);

#endif // UTILS_H_
