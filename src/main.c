/* Copyright (C) 2024 Marie "armillaire" Z. */
/* Check the included LICENSE file for more information. */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"

#define true 1
#define false 0

#define BF_COMP_NO_ARGS 1

void interactive_mode(void);

int
main(int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "error: expected 1 argument.");
		exit(BF_COMP_NO_ARGS);
	}
	
	if (strcmp("-i", argv[1]) == 0) {
		interactive_mode();
	} else {
		fprintf(stderr, "unimplemented.");
	}

	return 0;
}

void
interactive_mode(void) {
	while (true) {
	        char user_input[512];

		printf("> ");
		fgets(user_input, 512, stdin);
		char *stripped = strip_extra_characters(user_input);
		if (stripped == NULL)
			continue;
		
		printf("%s\n", stripped);
		free(stripped);
	}
}
