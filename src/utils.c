/* Copyright (C) 2024 Marie "armillaire" Z. */
/* Check the included LICENSE file for more information. */

#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

char*
strip_extra_characters(char *input) {
	size_t string_length = strnlen(input, 512);
	char *output_string = (char *)malloc(string_length * sizeof(char));
	size_t output_string_size = 0;

	for (size_t i = 0; i < string_length; i++) {
		char c = input[i];

		/* inefficient check to see if the character is valid */
		if (c != '<' && c != '>' && c != '.' && c != ',' && c != '[' &&
		    c != ']' && c != '+' && c != '-')
			continue;

	        output_string[output_string_size++] = c;
	}

	if (output_string_size == 0)
		return NULL;
	
	output_string = (char *)realloc(output_string, output_string_size + 1);
	output_string[output_string_size] = '\0';
	return output_string;
}
