CC=tcc
CFLAGS=-Wextra -Wall -Wpedantic
files=src/main.c src/utils.c

build:
	$(CC) $(CFLAGS) $(files) -o bf_comp

debug:
	clang $(CFLAGS) $(files) -ggdb -O0 -o bf_comp
